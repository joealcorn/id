from os import environ
import json

from postgres import Postgres
from postgres.cursors import SimpleDictCursor
import requests

db = Postgres(
    url=environ['DB_URL'],
    minconn=int(environ['DB_MINCONN']),
    maxconn=int(environ['DB_MAXCONN']),
    cursor_factory=SimpleDictCursor,
)

ACCESS_TOKEN = environ['ACCESS_TOKEN']
BASE_URL = 'https://api.github.com'


class Repo(object):

    def __init__(self, username, repo, token=None, **kw):
        self.username = username
        self.repo = repo
        self.token = token
        for k, v in kw.items():
            setattr(self, k, v)

    @classmethod
    def active(cls):
        for repo in db.all('SELECT * FROM repos where active is true'):
            yield Repo(**repo)

    def disable(self):
        print 'Disabling %s/%s' % (self.username, self.repo)
        db.run("UPDATE repos set active = 'f' where username = %s and repo = %s",
            (self.username, self.repo)
        )

    def open_count(self):
        url = BASE_URL + '/repos/%s/%s' % (self.username, self.repo)
        if not hasattr(self, '_open_count_'):
            r = self.get(url)
            if r.status_code == 404:
                return self.disable()

            elif r.status_code != 200:
                print r.status_code, r.json()['message']
                return None

            self._open_count_ = r.json()['open_issues_count']
        return self._open_count_

    def latest(self, exclude='sandbox only'):
        url = BASE_URL + '/repos/%s/%s/issues' % (self.username, self.repo)

        r = self.get(url, params={
            'sort': 'created',
            'direction': 'desc',
            'per_page': 20,
        })
        if r.status_code in (404, 410):
            print r.json()['message']
            self.disable()
            return []

        elif r.status_code != 200:
            print r.status_code
            return []

        data = []
        for issue in r.json():
            labels = set()
            for label in issue.get('labels', []):
                labels.add(label['name'])

            if exclude not in labels:
                data.append(issue)

        return data

    def get(self, url, **kw):
        if 'params' in kw:
            kw['params']['access_token'] = self.token
        else:
            kw['params'] = {
                'access_token': self.token
            }

        return requests.get(url, **kw)


def collect(repo):
    print 'Collecting things for %s/%s' % (repo.username, repo.repo)
    issues = {
        'open': repo.open_count(),
        'latest': repo.latest()[:6],
    }

    db.run('''
        INSERT INTO issues (data, username, repo) VALUES
        (%s, %s, %s)''', (json.dumps(issues), repo.username, repo.repo))


if __name__ == '__main__':
    from raven import Client
    client = Client(environ.get('SENTRY_DSN'))

    for repo in Repo.active():
        try:
            collect(repo)
        except Exception as e:
            print e
            client.captureException()
            continue
