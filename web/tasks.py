from web import celery, db
import requests
from psycopg2 import IntegrityError

base = 'https://api.github.com'

@celery.task
def import_repos(token):
    # get repos
    r = requests.get(base + '/user/repos', params={'access_token': token})
    assert r.status_code == 200

    for repo in r.json() + get_org_repos(token):
        if not repo['has_issues']:
            'Skipping %s: issue disabled' % repo['full_name']
            continue

        username, repo_name = repo['full_name'].split('/')
        private = repo['private']

        try:
            db.run('''
                INSERT INTO repos (username, repo, token, private) VALUES
                (%s, %s, %s, %s)
            ''', (username, repo_name, token, private))

        except IntegrityError:
            db.run(
                "UPDATE repos SET token = %s, private = %s, active = 't' WHERE username = %s and repo = %s",
                (token, private, username, repo_name)
            )

        print repo['full_name']


def get_org_repos(token):
    url = base + '/user/orgs'
    r = requests.get(url, params={'access_token': token})
    if r.status_code != 200:
        return []

    repos = []
    for org in r.json():
        url = base + '/orgs/%s/repos' % org['login']
        r = requests.get(url, params={'access_token': token})
        if r.status_code != 200:
            continue

        repos.extend(r.json())
        
    return repos

