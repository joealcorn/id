from os import urandom, environ
from urllib import urlencode

from flask import redirect, session, request, flash, url_for, render_template, jsonify
import requests

from web import app, tasks
from web.models import Repository


@app.route('/')
def index():
    return render_template('index.html', repos=Repository.latest())


@app.route('/import')
def import_repos():
    qs = {
        'scope': 'repo',
        'redirect_uri': environ['REDIRECT_URI'],
        'state': urandom(8).encode('hex'),
        'client_id': environ['CLIENT_ID'],
    }

    session['state'] = qs['state']

    return redirect('https://github.com/login/oauth/authorize?' + urlencode(qs))


@app.route('/auth')
def auth():
    session['state'] = None
    code = request.args.get('code', None)
    if not code:
        return 'You have to access the request silly'

    r = requests.post('https://github.com/login/oauth/access_token', {
        'client_id': environ['CLIENT_ID'],
        'client_secret': environ['CLIENT_SECRET'],
        'code': code,
        'redirect_uri': environ['REDIRECT_URI'],
    }, headers={'Accept': 'application/json'})

    token = r.json()['access_token']
    tasks.import_repos.delay(token)

    flash('Your repositories are being added to the index')
    return redirect(url_for('index'))


@app.route('/p/<username>/<repo_name>/')
def repo_view(username, repo_name):
    repo = Repository.get_or_404(username, repo_name)
    return render_template('dash.html', repo=repo, title=repo.fullname)


@app.route('/p/<username>/<repo_name>/data')
def chart(username, repo_name):
    repo = Repository.get_or_404(username, repo_name)
    return jsonify(data=repo.open_count())


@app.route('/p/<username>/<repo_name>/latest')
def latest(username, repo_name):
    repo = Repository.get_or_404(username, repo_name)
    return jsonify(data=repo.latest_issues())
