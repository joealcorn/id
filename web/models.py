import json

from web import db
from flask import abort, url_for


class Repository(object):

    __safe__ = ['username', 'name', 'fullname', 'private', 'active']

    def __init__(self, username, repo, token=None, **kw):
        self.username = username
        self.name = repo
        self.token = token
        self.fullname = '%s/%s' % (self.username, self.name)
        for k, v in kw.items():
            setattr(self, k, v)

    @classmethod
    def latest(cls, limit=10):
        repos = db.all("SELECT * FROM repos where active = 't' and private = 'f' LIMIT %s", [limit])
        for repo in repos:
            yield Repository(**repo)

    @classmethod
    def get_or_404(cls, username, repo):
        repo = db.one(
            'SELECT * FROM repos WHERE lower(username) = %s and lower(repo) = %s',
            (username.lower(), repo.lower())
        )
        if repo is None:
            abort(404)

        return cls(**repo)

    def url(self):
        return url_for('repo_view', username=self.username, repo_name=self.name)

    def latest_issues(self):
        issues = db.one(
            '''SELECT * FROM issues WHERE lower(username) = %s and lower(repo) = %s
            ORDER BY created DESC LIMIT 1''',
            (self.username.lower(), self.name.lower()))

        if not issues:
            return None

        issues['data'] = json.loads(issues['data'])
        return issues

    def open_count(self):
        issues = db.all(
            '''SELECT data, created from issues where lower(username) = %s and lower(repo) = %s
            ORDER BY created desc limit 14''', (self.username.lower(), self.name.lower())
        )

        count = []
        for issue in issues:
            count.append({
                'timestamp': issue['created'].strftime('%d %b'),
                'open': json.loads(issue['data'])['open'],
            })

        return count

    @property
    def template_context(self):
        ctx = {k: self.__dict__[k] for k in self.__safe__}
        return json.dumps(ctx)
