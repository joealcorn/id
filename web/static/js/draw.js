var draw_chart = function(incoming) {
    $('#chart').attr('width', $('#chart').parent()[0].offsetWidth - 20)

    incoming = incoming.data
    var data = {
        labels: [],
        datasets: [{
            data: [],
            fillColor : "rgba(220,220,220,0.5)",
            strokeColor : "rgba(220,220,220,1)",
            pointColor : "rgba(220,220,220,1)",
            pointStrokeColor : "#fff",
        }],

    }

    for (var d in incoming) {
        // chrome hangs if labels exceed 16 chars
        data.labels.unshift(incoming[d].timestamp.substring(0, 15));
        data.datasets[0].data.unshift(incoming[d].open);
    }

    var ctx = document.getElementById("chart").getContext("2d");
    var chart = new Chart(ctx).Line(data);
};


var latest_issues = function(incoming) {
    incoming = incoming.data;

    if (incoming === null || incoming.data.latest.length === 0) {
        var html = Mustache.render($('#no-issues-template').html(), window.repo);
        $('.issues:eq(-2)').append(html)
        return;
    }

    var issues = incoming.data.latest;
    for (var i in issues) {
        if (i % 3 === 0) {
            // insert another row for issues to go in
            var row = Mustache.render($('#issue-row-template').html());
            $('.container').append(row);
        }

        var issue = issues[i];
        var view = {
            html_url: issue.html_url,
            number: issue.number,
            title: issue.title,
            user: issue.user,
            created_at: moment(issue.created_at).fromNow(),
        }

        var html = Mustache.render($('#issue-template').html(), view)
        // select the last element that isn't the template
        $('.issues:eq(-2)').append(html)

        $('a').tooltip({
            html: true,
            placement: 'auto bottom',
        });

    }
};

$(document).ready(function() {
    // get chart data
    $.ajax({
        dataType: 'json',
        url: 'data',
        success: draw_chart
    });

    // get latest issues
    $.ajax({
        dataType: 'json',
        url: 'latest',
        success: latest_issues
    });
});

