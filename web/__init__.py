from os import environ, urandom

from flask import Flask
from postgres.cursors import SimpleDictCursor
from postgres import Postgres

app = Flask(__name__)
app.config['DEBUG'] = environ.get('DEBUG', '0') == '1'
app.config['SECRET_KEY'] = environ.get('SECRET_KEY', urandom(16).encode('hex'))


if not app.debug:
    import logging
    from logging.handlers import StreamHandler
    handler = StreamHandler()
    handler.setLevel(logging.ERROR)
    app.logger.addHandler(handler)

from raven.contrib.flask import Sentry
from raven.contrib.celery import register_signal

sentry = Sentry(app)
register_signal(sentry.client)


db = Postgres(
    url=environ['DB_URL'],
    minconn=int(environ['DB_MINCONN']),
    maxconn=int(environ['DB_MAXCONN']),
    cursor_factory=SimpleDictCursor,
)

from web.make_celery import celery  # noqa
from web import views  # noqa
