CREATE TABLE repos (
    username text,
    repo text,
    token text NULL,
    private boolean,
    active boolean default true,
    PRIMARY KEY (username, repo)
);

CREATE TABLE issues (
    id serial PRIMARY KEY,
    data text,
    username text,
    repo text,
    created timestamp without time zone default now(),
    FOREIGN KEY (username, repo) REFERENCES repos (username, repo) ON DELETE CASCADE
);
